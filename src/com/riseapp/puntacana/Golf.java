package com.riseapp.puntacana;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class Golf extends FragmentActivity {
	
    RelativeLayout generalFrame;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_golf);
		generalFrame = (RelativeLayout) findViewById(R.id.generalFrame);
		
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		
		GolfStartFragment myFragment = new GolfStartFragment();
		ft.add(R.id.golfFragment, myFragment);
		ft.commit();
		
		generalFrame.setOnTouchListener(new OnSwipeTouchListener() {
			
		    public void onSwipeTop() {
		    	Log.i("Swipe", "SwipeTop");
		    }
		    public void onSwipeRight() {
		    	Log.i("click boton", "HIT!");
				Fragment nuevoFragment;
				nuevoFragment = new GolfFragment1();
				FragmentTransaction trans = getSupportFragmentManager().beginTransaction();
				trans.replace(R.id.golfFragment, nuevoFragment);
				trans.addToBackStack(null);
				trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				trans.commit();
				
		    }
		    public void onSwipeLeft() {
		    	Log.i("Swipe", "SwipeLeft");
		    }
		    public void onSwipeBottom() {
		    	Log.i("Swipe", "SwipeBottom");
		    }
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_golf, menu);
		return true;
	}
	
}