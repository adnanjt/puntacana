package com.riseapp.puntacana;



import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.support.v4.app.NavUtils;

import android.app.ListActivity;

public class MenuInicial extends Activity {

	ImageButton ibtnRE, ibtnLS, ibtnG, ibtnDB, ibtnH, ibtnW, ibtnS, ibtnAF;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu_inicial);
		Log.i("paso", "paso por aqui");
		ibtnRE = (ImageButton) findViewById(R.id.imageButtonRE);
		ibtnLS = (ImageButton) findViewById(R.id.imageButtonLS);
		ibtnG = (ImageButton) findViewById(R.id.imageButtonG);
		ibtnDB = (ImageButton) findViewById(R.id.imageButtonDB);
		ibtnH = (ImageButton) findViewById(R.id.imageButtonH);
		ibtnW = (ImageButton) findViewById(R.id.imageButtonW);
		ibtnS = (ImageButton) findViewById(R.id.imageButtonS);
		ibtnAF = (ImageButton) findViewById(R.id.imageButtonAF);
		
		ibtnRE.setOnClickListener(ibtnOnClickListener);
		ibtnLS.setOnClickListener(ibtnOnClickListener);
		ibtnG.setOnClickListener(ibtnOnClickListener);
		ibtnDB.setOnClickListener(ibtnOnClickListener);
		ibtnH.setOnClickListener(ibtnOnClickListener);
		ibtnW.setOnClickListener(ibtnOnClickListener);
		ibtnS.setOnClickListener(ibtnOnClickListener);
		ibtnAF.setOnClickListener(ibtnOnClickListener);
		
	}
	
	View.OnClickListener ibtnOnClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent openNextActivity; 
			
			
			
			if(v == ibtnRE){
				openNextActivity = new Intent("com.riseapp.puntacana.REALESTATE");
			}else if(v == ibtnLS){
				openNextActivity = new Intent("com.riseapp.puntacana.LIFESTYLE");
			}else if(v == ibtnG){
				openNextActivity = new Intent("com.riseapp.puntacana.GOLF");
			}else if(v == ibtnDB){
				openNextActivity = new Intent("com.riseapp.puntacana.DININGBAR");
			}else if(v == ibtnH){
				openNextActivity = new Intent("com.riseapp.puntacana.HOSPITALITY");
			}else if(v == ibtnW){
				openNextActivity = new Intent("com.riseapp.puntacana.WEDDING");
			}else if(v == ibtnS){
				openNextActivity = new Intent("com.riseapp.puntacana.SPA");
			}else if(v == ibtnAF){
				openNextActivity = new Intent("com.riseapp.puntacana.AIRPORTFLIGHTS");
			}else{
				openNextActivity = new Intent("com.riseapp.puntacana.MENUINICIAL");
			}
			
			startActivity(openNextActivity); 
			
		}
		
		
	};




	

//	@Override
//	public boolean onCreateOptionsMenu(android.view.Menu menu) {
//	
//		boolean r = super.onCreateOptionsMenu(menu);
//		MenuInflater blowUp = getMenuInflater();
//		blowUp.inflate(R.menu.cool_menu, menu);
//		return r;
//
//	}

//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		// TODO Auto-generated method stub
//		// return super.onOptionsItemSelected(item);
//		if (item.getItemId() == R.id.aboutUs) {
//			Intent i = new Intent("com.thenewboston.travis.ABOUT");
//			startActivity(i);
//		} else if (item.getItemId() == R.id.preferences) {
//			Intent p = new Intent("com.thenewboston.travis.PREFS");
//			startActivity(p);
//		} else if (item.getItemId() == R.id.exit) {
//			finish();
//				
//		}
//		return false;
//	}
//	

}
