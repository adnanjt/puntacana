package com.riseapp.puntacana;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class Hospitality extends Activity {

	ImageButton enviarMail;
	ImageButton llamar;
	String[] mailAddress = {"ajt_tavarez@hotmail.com"};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_hospitality);
		
		enviarMail = (ImageButton) findViewById(R.id.bMailHospitality);
		enviarMail.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mailIntent = new Intent(android.content.Intent.ACTION_SEND);
				mailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, mailAddress);
				mailIntent.setType("plain/text");
				try {
				    startActivity(Intent.createChooser(mailIntent, "Send mail..."));
				} catch (android.content.ActivityNotFoundException ex) {
				    Toast.makeText(Hospitality.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		llamar = (ImageButton) findViewById(R.id.bLlamarHospitality);
		llamar.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent llamarIntent = new Intent(android.content.Intent.ACTION_CALL);
				llamarIntent.setData(Uri.parse(getString(R.string.telefonoHospitality)));
				startActivity(llamarIntent);
			}
		});
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_hospitality, menu);
		return true;
	}

}
